<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan_detail extends Model
{
    public function Detail_buku()
	{
	      return $this->belongsTo('App\Detail_buku','Detail_buku_id', 'id');
	}

	public function pesanan()
	{
	      return $this->belongsTo('App\pesanan','pesanan_id', 'id');
	}
}