<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail_buku extends Model
{
    public function Pesanan_detail()
    {
        return $this->hasMany('App\Pesanan_detail','Detail_buku_id','id');
    }    
}
